# rangeure.asvt.ch

Hierbei handelt es sich um ein [Jekyll](https://jekyllrb.com/) template für den Armbrustschützenverein Turbenthal.
Damit lässt sich eine vereinsinterne Webseite generieren, die folgende Funktionen enthält:

* Auflistung aller Schützenfeste und deren Teilnehmer
* Übersicht Rangeure für die einzelnen Schützenfeste
* Persönliche Übersicht über die eigenen Rangeure

Das Original-Setup stammt von der [Digitalen Gesellschaft Schweiz](https://github.com/DigitaleGesellschaft/jekyll-conference) ([MIT-Lizenz](https://github.com/DigitaleGesellschaft/jekyll-conference/blob/0205cc0ad38efb65b869fbe48cbb04e7e672dc5a/LICENSE.md)) und wurde für unsere Zwecke angepasst.

## Setup

Live view:

```shell
make run
```

Build:

```shell
make build
```

## Content

* Jedes Schützenfest wird durch eine Datei in `_schuetzenfeste/` repräsentiert.
* Jeder Schütze wird durch eine Datei in `_schuetzen/` repräsentiert.