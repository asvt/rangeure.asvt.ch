JEKYLL_IMAGE_VERSION=4.2.2
DOCKER_OPTION=--rm --platform linux/amd64 --name jekyll --volume="$(PWD):/srv/jekyll:Z"

build:
	docker run $(DOCKER_OPTION) --interactive jekyll/builder:$(JEKYLL_IMAGE_VERSION) jekyll build

bundle-update:
	docker run $(DOCKER_OPTION) --publish 127.0.0.1:4000:4000 jekyll/jekyll:$(JEKYLL_IMAGE_VERSION) bundle update

run:
	docker run $(DOCKER_OPTION) --publish 127.0.0.1:4000:4000 jekyll/jekyll:$(JEKYLL_IMAGE_VERSION) jekyll serve --trace

stop:
	docker stop --time 1 jekyll
