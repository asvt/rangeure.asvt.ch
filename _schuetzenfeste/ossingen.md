---
name: Freundschaftsschiessen
datum: 25.04.2025, 03.05.2025
location: Ossingen
website: https://www.asv-ossingen.ch/
einladung: https://cloud.asvt.ch/index.php/s/2zx5rzW56tkX8AG
auszahlung:
status: Angemeldet
schuetzen:
  - schuetze: Hans Bührer
    rangeur: 25.04.2025 15:00
  - schuetze: Max Leuenberger
    rangeur: 25.04.2025 15:00

  - schuetze: Dominic Mötteli
    rangeur: 25.04.2025 17:30
  - schuetze: Philipp Geitner
    rangeur: 25.04.2025 17:30
  - schuetze: Markus Ritzmann
    rangeur: 25.04.2025 17:30

  - schuetze: Chantal Guntern
    rangeur: 03.05.2025 11:00
  - schuetze: Tulio Zehnder
    rangeur: 03.05.2025 11:00

  - schuetze: Christian Hefti
    rangeur: 03.05.2025 11:30
  - schuetze: Robert Hiestand
    rangeur: 03.05.2025 11:30
unbekannt:
  # unbekannt
  - schuetze: Andrin Meier
  - schuetze: Dominic Bernet
  - schuetze: Florian Karrer
  - schuetze: Jan Mika Barbagallo
  - schuetze: Joshua Guntern
  - schuetze: Markus Wüest
  - schuetze: Michael Crepaldi
  - schuetze: Nicola Riexinger
  - schuetze: Nicole Barbagallo
  - schuetze: Reto Bieri
  - schuetze: Siro Stauffer
  - schuetze: Tabea Bernet
absage:
  - schuetze: Nevio Corti
categories:
  - April
---

Ossingen kennt keine Rangeure. Ihr könnt auch früher, später oder an einem anderen Schiesstag schiessen.
Die Anmeldung ist aber trotzdem verbindlich, da die Scheiben vorbereitet werden.

Schiesstage:

* Freitag 25.04.2025, ab 14:00
* Samstag 03.05.2025, 09:00 bis 16:00 mit Absenden