---
name: Dickischiessen
datum: 09-11.05.2025, 16-18.05.2025
location: Krichenwil
website: https://www.asgkriechenwil.ch/dickischiessen
einladung: https://cloud.asvt.ch/index.php/s/NMTdbZtH6c5ffb2
auszahlung:
status: Angemeldet
schuetzen:
  - schuetze: Philipp Geitner
    rangeur: 16.05.2025 20:45
  - schuetze: Christian Hefti
    rangeur: 16.05.2025 20:45
  - schuetze: Dominic Mötteli
    rangeur: 16.05.2025 20:45
  - schuetze: Markus Ritzmann
    rangeur: 16.05.2025 20:45
  - schuetze: Robert Hiestand
    rangeur: 16.05.2025 21:00
unbekannt:
  # unbekannt
  - schuetze: Andrin Meier
  - schuetze: Chantal Guntern
  - schuetze: Dominic Bernet
  - schuetze: Florian Karrer
  - schuetze: Jan Mika Barbagallo
  - schuetze: Joshua Guntern
  - schuetze: Markus Wüest
  - schuetze: Michael Crepaldi
  - schuetze: Nevio Corti
  - schuetze: Nicola Riexinger
  - schuetze: Nicole Barbagallo
  - schuetze: Reto Bieri
  - schuetze: Siro Stauffer
  - schuetze: Tabea Bernet
  - schuetze: Tulio Zehnder
absage:
  - schuetze: Hans Bührer
  - schuetze: Max Leuenberger
categories:
  - Mai
---