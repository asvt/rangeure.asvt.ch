---
name: ZKAV Verbanschiessen
datum: 06.06.2025, 13-15.06.2025
location: Seen-Gotzenwil
website: https://www.asv-seen-gotzenwil.ch/
einladung: https://cloud.asvt.ch/index.php/s/4xBGZGzcDqyAFLZ
auszahlung:
status: Angemeldet, Bestätigung ausstehend
schuetzen:

  - schuetze: Hans Bührer
    rangeur: 06.06.2025 15:00
  - schuetze: Max Leuenberger
    rangeur: 06.06.2025 15:00
  - schuetze: Philipp Geitner
    rangeur: 06.06.2025 15:00
  - schuetze: Christian Hefti
    rangeur: 06.06.2025 15:00

  - schuetze: Reto Bieri
    rangeur: 06.06.2025 16:00 (Zeitlich nach Christian)

  - schuetze: Markus Wüest
    rangeur: 06.06.2025 18:00

  - schuetze: Jan Mika Barbagallo
    rangeur: 13.06.2025 16:30
  - schuetze: Nicole Barbagallo
    rangeur: 13.06.2025 17:30 (Zeitlich nach Jan Mika)

  - schuetze: Dominic Mötteli
    rangeur: 14.06.2025 12:30

  - schuetze: Michael Crepaldi
    rangeur: 14.06.2025 10:00

  - schuetze: Chantal Guntern
    rangeur: 14.06.2025 15:00

unbekannt:
  # mit rest anmelden
  - schuetze: Robert Hiestand

  # unbekannt
  - schuetze: Andrin Meier
  - schuetze: Siro Stauffer
  - schuetze: Tabea Bernet
  - schuetze: Tulio Zehnder
  - schuetze: Dominic Bernet
  - schuetze: Florian Karrer
  - schuetze: Joshua Guntern
  - schuetze: Markus Ritzmann
  - schuetze: Nevio Corti
  - schuetze: Nicola Riexinger
absage:
categories:
  - Juni
---