---
name: Jubiläumsschiessen
datum: 23.05.2025
location: Oberengstringen
website: https://www.asoberengstringen.ch/jubilaeumsschiessen/
einladung: https://cloud.asvt.ch/index.php/s/oYj4TN87DAtAcey
auszahlung:
status: Angemeldet
schuetzen:
  - schuetze: Markus Wüest
    rangeur: 23.05.2025 15:45
unbekannt:
absage:
categories:
  - Mai
---