---
name: Jubiläumsschiessen
datum: 20-22.06.2015, 27-29.06.2015
location: Oberwil
website: https://www.asv-oberwil.ch/
einladung: https://cloud.asvt.ch/index.php/s/3FPHJ5acD5FxQm8
auszahlung:
status: Angemeldet
schuetzen:
  - schuetze: Reto Bieri
    rangeur: 20.06.2025 13:00
  - schuetze: Christian Hefti
    rangeur: 20.06.2025 14:15 (Zeitlich nach Reto)

  - schuetze: Philipp Geitner
    rangeur: 27.06.2025 13:00
  - schuetze: Markus Wüest
    rangeur: 27.06.2025 13:00

  - schuetze: Hans Bührer
    rangeur: 27.06.2025 15:00
  - schuetze: Max Leuenberger
    rangeur: 27.06.2025 15:00

  - schuetze: Jan Mika Barbagallo
    rangeur: 27.06.2025 17:00
  - schuetze: Nicole Barbagallo
    rangeur: 27.06.2025 18:00 (Zeitlich nach Jan Mika)

  - schuetze: Robert Hiestand
    rangeur: 29.06.2025 09:00
  - schuetze: Dominic Mötteli
    rangeur: 29.06.2025 09:00
  - schuetze: Michael Crepaldi
    rangeur: 29.06.2025 09:00
  - schuetze: Nevio Corti
    rangeur: 29.06.2025 09:00
  - schuetze: Tabea Bernet
    rangeur: 29.06.2025 09:00
  - schuetze: Tulio Zehnder
    rangeur: 29.06.2025 09:00
    # Hat 09:15 angemeldet. Nehme an, da Zeit für einrichtung/hilfe für ehemals Jungschützen benötigt wird.
    # Habe deshalb +1 Rangeur hinzugefügt, da beginn 09:15 nicht möglich sein wird.
  - schuetze: Chantal Guntern
    rangeur: 29.06.2025 09:00 (+1 Rangeur hinzugefügt)
unbekannt:
  - schuetze: Markus Ritzmann
  - schuetze: Nicola Riexinger
  - schuetze: Joshua Guntern
  - schuetze: Florian Karrer
  - schuetze: Dominic Bernet
  - schuetze: Andrin Meier
  - schuetze: Siro Stauffer
absage:
categories:
  - Juni
---