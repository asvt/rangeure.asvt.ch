---
name: Vereinsausflug
datum: 24.05.2025
location: Oberengstringen & Herisau-Waldstatt
website:
einladung: https://cloud.asvt.ch/index.php/s/Z8s9Ew8WFfCHAyX
auszahlung:
status: Angemeldet
schuetzen:

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Biberstich angemeldet
  - schuetze: Nicola Riexinger
    rangeur: 24.05.2025

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Dominic Mötteli
    rangeur: 24.05.2025

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Markus Ritzmann
    rangeur: 24.05.2025

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Reto Bieri
    rangeur: 24.05.2025

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Biberstich angemeldet
  - schuetze: Christian Hefti
    rangeur: 24.05.2025

  # Oberengstringen: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Kranz angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Biberstich angemeldet
  - schuetze: Michael Crepaldi
    rangeur: 24.05.2025

  # Oberengstringen: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Kranz angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Philipp Geitner
    rangeur: 24.05.2025

  # Oberengstringen: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Kranz angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Biberstich angemeldet
  - schuetze: Joshua Guntern
    rangeur: 24.05.2025

  # Oberengstringen: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Kranz angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Robert Hiestand
    rangeur: 24.05.2025

  # Oberengstringen: Nicht gefragt. Sektion, Auszahlung, Juniorenstiche angemeldet
  # Herisau-Waldstatt: Nicht gefragt. Sektion, Auszahlung, Biberstich
  - schuetze: Nevio Corti
    rangeur: 24.05.2025

  # Oberengstringen: Nicht gefragt. Sektion, Auszahlung, Juniorenstiche angemeldet
  # Herisau-Waldstatt: Nicht gefragt. Sektion, Auszahlung, Biberstich
  - schuetze: Tulio Zehnder
    rangeur: 24.05.2025

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Biberstich angemeldet
  - schuetze: Andrin Meier
    rangeur: 24.05.2025 (ohne Herisau-Waldstatt)

  # Oberengstringen: Stiche unbekannt. Sektion, Gruppe, Auszahlung, Kranz angemeldet
  # Herisau-Waldstatt: Stiche unbekannt. Sektion, Gruppe, Auszahlung angemeldet
  - schuetze: Max Leuenberger # ohne Ausflug
    rangeur: 24.05.2025 (ohne Nachmittagsprogramm)

  # Oberengstringen: Normal angemeldet
  # Herisau-Waldstatt: Normal angemeldet
  - schuetze: Hans Bührer # ohne Ausflug
    rangeur: 24.05.2025 (ohne Nachmittagsprogramm)
unbekannt:
  # hat per WhatsApp nicht geantwortet
  # tel nicht erreicht
  - schuetze: Dominic Bernet
absage:
  - schuetze: Markus Wüest
  - schuetze: Nicole Barbagallo
  - schuetze: Siro Stauffer
  - schuetze: Florian Karrer
  - schuetze: Tabea Bernet
  - schuetze: Chantal Guntern
  - schuetze: Jan Mika Barbagallo
categories:
  - Mai
---

Wir kombinieren zwei Feste.