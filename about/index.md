---
layout: default
title: About
---

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">About</li>
    </ol>
  </nav>

<h1>About</h1>

<h2>Verein</h2>

* IBAN: CH78 0900 0000 8400 8489 6
* PC-Konto: 84-8489-6

<h2>Software</h2>

* [Git Repository](https://gitlab.com/asvt/rangeure.asvt.ch)